#ifndef ROTATE_H
#define ROTATE_H


/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image  source );

void rotate_image(struct image* image, int angle);

#endif //ROTATE_H
