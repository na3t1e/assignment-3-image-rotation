#ifndef PIXEL_H
#define PIXEL_H
#include <inttypes.h>

struct pixel {
     uint8_t b, g, r;
};

#endif //PIXEL_H
