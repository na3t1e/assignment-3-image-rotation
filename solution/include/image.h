#ifndef IMAGE_H
#define IMAGE_H
#include "pixel.h"
#include <inttypes.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct image create_image(uint64_t const width, uint64_t const height);
#endif //IMAGE_H
