#include "bmp.h"
#include "reader.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;


    fseek(in, 0, SEEK_END);
    const uint32_t fsize = ftell(in);
    rewind(in);

    if (!fread(&header, sizeof(header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    if (fsize != header.bfileSize) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biSize != 40) {
        return READ_INVALID_HEADER;
    }
    if ((header.biBitCount != 24) || (header.biCompression != 0)) {
        return READ_INVALID_BITS;
    }
    *img = create_image(header.biWidth, header.biHeight);
    const uint8_t pixel_size = sizeof(struct pixel);
    if (img->data == NULL) {
        printf("Insufficient memory available"
            "\n");
        return READ_INVALID_HEADER;
    }
    int padding = 0;
    if (header.biWidth * pixel_size % 4 != 0) {
        padding = (int) (4 - (header.biWidth * pixel_size) % 4);
    }

    fseek(in, header.bfOffBits, SEEK_SET);
    for (uint32_t i = 1; i <= img->height; ++i) {
        fread(&img->data[(header.biHeight - i) * header.biWidth], pixel_size, header.biWidth, in);
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}


enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header;
    uint8_t padding = 0;
    const uint8_t pixel_size = sizeof(struct pixel);
    if (img->width*pixel_size % 4 != 0) {
        padding = 4 - img->width * pixel_size % 4;
    }
    header.bfType = 0x4D42;
    header.bfReserved = 0;
    header.bfileSize = sizeof(struct bmp_header) + (img->width *pixel_size + padding) * img->height;
    header.bfOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = pixel_size * 8;
    header.biCompression = 0;
    header.biSizeImage = img->width * img->height * pixel_size;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    if (!fwrite(&header, sizeof(header), 1, out)) {
        return WRITE_ERROR;
    }

    uint32_t pad = 0;
    for (uint32_t i = 1; i <= img->height; ++i) {
        if (fwrite(&img->data[(img->height - i) * img->width],  pixel_size, img->width, out) < img->width) {
            return WRITE_ERROR;
        }
        if (padding && fwrite(&pad, 1, padding, out) < padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

