#include "image.h"
#include "pixel.h"
#include <stdio.h>
#include <stdlib.h>

struct image create_image(uint64_t const width, uint64_t const height) {
    const struct image img = {width, height, malloc(height * width * sizeof(struct pixel))};
    if (img.data == NULL) {
        fprintf(stderr, "Cannot allocate memory for image."
                "\n");
        exit(1);
    }
    return img;
}
