#include <stdlib.h>

#include "image.h"

static void rotate(struct image *source) {
    const struct image rotated = create_image(source->height, source->width);
    for (int i = 0; i < rotated.height; i++) {
        for (int j = 0; j < rotated.width; j++) {
            rotated.data[i * rotated.width + j] = source->data[(j + 1) * source->width - i - 1];
        }
    }
    free(source->data);
    source->data = rotated.data;
    source->width = rotated.width;
    source->height = rotated.height;
}

void rotate_image(struct image *image, const int angle) {
    for (int count = (4 - (angle / 90) % 4), i = 0; i < count; i++)
        rotate(image);
}

