#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"
#include "reader.h"
#include "rotate.h"

int main(const int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Wrong amount of arguments."
                "\n");
        exit(1);
    }

    const char *source_path = argv[1];
    const char *transform_path = argv[2];
    const char *char_angle = argv[3];
    const int angle = atoi(char_angle);

    if (angle % 90 != 0) {
        fprintf(stderr, "Wrong angle."
                "\n");
        exit(1);
    }

    FILE *image_file = fopen(source_path, "rb");
    if (image_file == NULL) {
        fprintf(stderr, "Cannot open file to read."
                "\n");
        exit(1);
    }

    struct image image;
    const enum read_status r_status = from_bmp(image_file, &image);
    if (r_status != READ_OK) {
        fprintf(stderr, "Error while reading bmp."
                "\n"
                "Information: %i"
                "\n", r_status);
        exit(1);
    }
    fclose(image_file);
    rotate_image(&image, angle);

    if ((image_file = fopen(transform_path, "wb")) == NULL) {
        fprintf(stderr, "Cannot open file to write."
                "\n");
        exit(1);
    }

    const enum write_status w_status = to_bmp(image_file, &image);

    if (w_status != WRITE_OK) {
        fprintf(stderr, "Error while writing bmp."
                "\n"
                "Information: %i"
                "\n", w_status);
        exit(1);
    }
    fclose(image_file);
    free(image.data);
    return 0;
}
